<?php

return array(
    'sitename' => 'Тестовая страница php фреймворка',
    'db' => include 'config.db.php',
    'scripts' => array(
        '/assets/js/libs/jquery-2.1.3.min.js',
        '/assets/js/libs/bootstrap/js/bootstrap.min.js',
    ),
    'styles' => array(
        '/assets/js/libs/bootstrap/css/bootstrap.min.css',
        '/assets/js/libs/bootstrap/css/bootstrap-theme.min.css',
        '/assets/css/blog.css',
    ),
);