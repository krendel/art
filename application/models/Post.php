<?php

/**
 * Class Post
 */
class Post extends ModelDb
{
    /**
     * @var string
     */
    static public $table = 'posts';
    /**
     * @var array
     */
    public $safe = array('id', 'name', 'content');

    /**
     * @var integer
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $content;

    /**
     * @return bool
     */
    public function beforeSave()
    {
        if (strlen($this->name) < 3) {
            $this->errors['name'] = 'Слишком короткое название';
        }

        return parent::beforeSave();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }
}
