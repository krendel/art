<?php

/**
 * Class User
 */
class User extends Model
{
    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $surname;

    /**
     * @var array
     */
    public $safe = array('id', 'login', 'password', 'email');

    /**
     * @return string
     */
    function getFullName()
    {
        return $this->name.' '.$this->surname;
    }

    /**
     * @param $value
     */
    function setFullName($value)
    {
        list($this->name, $this->surname) = explode(' ', $value);
    }

    /**
     * Email validation
     * 
     * @param $value
     */
    function setEmail($value)
    {
        if (preg_match('#.+@.+#u', $value)) {
            $this->email = $value;
        }
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * @param string $surname
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;
    }

    /**
     * @return array
     */
    public function getSafe()
    {
        return $this->safe;
    }

    /**
     * @param array $safe
     */
    public function setSafe($safe)
    {
        $this->safe = $safe;
    }
    
}