<?php

/**
 * Class IndexController
 */
class IndexController extends Controller
{
    function actionIndex()
    {
        $this->renderPartial('index');
    }
}