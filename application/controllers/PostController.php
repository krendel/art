<?php

/**
 * Class PostController
 */
class PostController extends Controller
{
    function actionIndex()
    {
        $this->render('index', array('items' => Post::models()));
    }

    function actionCreate()
    {
        $post = new Post();
        if (isset($_POST['form'])) {
            $post->attributes = $_POST['form'];
            if ($post->save()) {
                header('location:/post');
            }
        }
        $this->render('form', array('item' => $post));
    }

    /**
     * @param $id
     */
    function actionRead($id)
    {
        $id = (int)$id;
        $post = Post::model($id);
        $this->render('read', array('item' => $post));
    }

    /**
     * @param $id
     * @throws Except
     */
    function actionUpdate($id)
    {
        $id = (int)$id ? (int)$id : (int)$_POST['form']['id'];
        $post = Post::model($id);
        if ($post->id) {
            if (isset($_POST['form'])) {
                $post->attributes = $_POST['form'];
                if ($post->save()) {
                    header('location:/post');
                }
            }
            $this->render('form', array('item' => $post));
        } else {
            throw new Except('Запись не найдена');
        }
    }
}