<?php

/**
 * Class Model
 */
abstract class Model
{
    /**
     * @var null|stdClass
     */
    protected $_data = null;

    /**
     * @var array
     */
    public $safe = array();

    /**
     * Model constructor.
     */
    function __construct()
    {
        $this->_data = new stdClass();
    }

    /**
     * @param $name
     * @param $value
     */
    function __set($name, $value)
    {
        if ($name === 'attributes') {
            foreach ($value as $key => $item) {
                $this->__set($key, $item);
            }

            return;
        }

        if (method_exists($this, 'set'.$name)) {
            call_user_func(array($this, 'set'.$name), $value);
        }

        if (in_array($name, $this->safe)) {
            $this->_data->$name = $value;
        }

        return $this->_data->$name;
    }

    /**
     * @param $name
     * @return mixed|null|stdClass
     */
    function __get($name)
    {
        if ($name === 'attributes') {
            return $this->_data;
        }
        if (method_exists($this, 'get'.$name)) {
            return call_user_func(array($this, 'get'.$name));
        }

        return property_exists($this->_data, $name) ? $this->_data->$name : null;
    }
}