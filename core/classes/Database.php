<?php

/**
 * Class Database
 */
class Database
{
    /**
     * @var null
     */
    private $stmt = null;
    /**
     * @var PDO
     */
    private $pdo;
    /**
     * @var null
     */
    private $sql = null;
    /**
     * @var bool
     */
    public $debug = true;

    /**
     * @var null
     */
    private static $_instance = null;

    /**
     * Database constructor.
     * @param $config
     */
    private function __construct($config)
    {
        if (!$config['charset']) {
            $config['charset'] = 'utf8';
        }
        $dsn = "mysql:host=".$config['host'].";dbname=".$config['dbname'].";charset=".$config['charset'];
        $opt = array(
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        );
        try {
            $this->pdo = new PDO($dsn, $config['user'], $config['password'], $opt);
        } catch (PDOException $e) {
            throw new PDOException("Error  : ".$e->getMessage());
        }
    }

    private function __clone()
    {
    }

    /**
     * @param $config
     * @return Database|null
     */
    public static function getInstance($config)
    {
        if (!isset(self::$_instance)) {
            self::$_instance = new self($config);
        }

        return self::$_instance;
    }

    /**
     * @param $sql
     * @param array $params
     * @return $this
     */
    function query($sql, $params = array())
    {
        $this->sql = $sql;
        $this->stmt = $this->pdo->prepare($this->sql);
        $this->stmt->execute(array_values($params));

        return $this;
    }

    /**
     * @param bool $field
     * @return null
     */
    function row($field = false)
    {
        if ($this->stmt && $row = $this->stmt->fetch(PDO::FETCH_LAZY)) {
            return $field ? $row[$field] : $row;
        }

        return null;
    }

    /**
     * @return mixed
     */
    function rows()
    {
        $rows = $this->stmt->fetchAll();

        return $rows;
    }

    /**
     * @param $table
     * @param array $fields
     * @return bool
     */
    public function insert($table, $fields = array())
    {
        if (count($fields)) {
            $fields = get_object_vars($fields);
            $keys = array_keys($fields);
            $values = '';
            $x = 1;
            foreach ($fields as $field) {
                $values .= '?';
                if ($x < count($fields)) {
                    $values .= ', ';
                }
                $x++;
            }
            $sql = "INSERT INTO {$table} (`".implode('`, `', $keys)."`) VALUES ({$values});";
            if (!$this->query($sql, $fields)) {
                return false;
            }
        }

        return true;
    }

    /**
     * @return string
     */
    function id()
    {
        return $this->pdo->lastInsertId();
    }

    /**
     * @param $table
     * @param $fields
     * @param int $where
     * @return bool
     */
    public function update($table, $fields, $where = 1)
    {
        $set = '';
        $x = 1;
        $fields = get_object_vars($fields);
        foreach ($fields as $name => $value) {
            $set .= "{$name} = ?";
            if ($x < count($fields)) {
                $set .= ', ';
            }
            $x++;
        }
        $sql = "UPDATE {$table} SET {$set} WHERE {$where}";
        if (!$this->query($sql, $fields)) {
            return false;
        }

        return true;
    }

    /**
     * @param $table
     * @param $where
     * @return Database
     */
    public function delete($table, $where)
    {
        return $this->query('DELETE FROM '.$table.' WHERE '.$where);
    }
}