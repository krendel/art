<?php

/**
 * Class Registry
 */
class Registry
{
    /**
     * @var array
     */
    private $data = array();

    /**
     * Registry constructor.
     * @param array $data
     */
    function __construct($data = array())
    {
        $this->data = $data;
    }

    /**
     * @param $name
     * @return mixed|null
     */
    function __get($name)
    {
        return isset($this->data[$name]) ? $this->data[$name] : null;
    }

    /**
     * @param $name
     * @param $value
     */
    function __set($name, $value)
    {
        $this->data[$name] = $value;
    }
}