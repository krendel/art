<?php

/**
 * Class Singleton
 */
abstract class Singleton
{
    /**
     * @var array
     */
    private static $instance = array();

    /**
     * @param bool $className
     * @return mixed
     * @throws Except
     */
    public static function getInstance($className = false)
    {
        $sClassName = ($className === false) ? get_called_class() : $className;
        if (class_exists($sClassName)) {
            if (!isset(self::$instance[$sClassName])) {
                self::$instance[$sClassName] = new $sClassName();
            }

            return self::$instance[$sClassName];
        } else {
            throw new Except('Class '.get_called_class().'  no exist!');
        }
    }

    /**
     * @param bool $className
     * @return mixed
     * @throws Except
     */
    public static function app($className = false)
    {
        return self::getInstance($className);
    }

    /**
     * Singleton clone.
     */
    private function __clone()
    {
    }

    /**
     * Singleton constructor.
     */
    private function __construct()
    {
    }
}