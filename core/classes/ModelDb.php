<?php

/**
 * Class ModelDb
 */
abstract class ModelDb extends Model
{
    /**
     * @var array
     */
    public $errors = array();
    /**
     * @var string
     */
    static public $table = '{table}';
    /**
     * @var string
     */
    static public $primary = 'id';

    /**
     * @return bool
     */
    function beforeSave()
    {
        return !count($this->errors);
    }

    /**
     * @return mixed
     */
    function save()
    {
        $modelname = get_called_class();
        if ($this->beforeSave()) {
            if (!$this->__get(self::$primary)) {
                $res = Art::app()->db->insert($modelname::$table, $this->_data);

                return $res;
            } else {
                return Art::app()->db->update(
                    $modelname::$table,
                    $this->_data,
                    $modelname::$primary.'='.$this->__get(self::$primary)
                );
            }
        }
    }

    /**
     * @return string
     */
    static function getQuery()
    {
        $modelname = get_called_class();

        return 'select * from '.$modelname::$table;
    }

    /**
     * @return array
     */
    static function models()
    {
        $items = Art::app()->db->query(self::getQuery())->rows();
        $results = array();
        $modelname = get_called_class();
        foreach ($items as $item) {
            $model = new $modelname();
            $model->attributes = $item;
            $results[] = $model;
        }

        return $results;
    }

    /**
     * @param $id
     * @return mixed
     */
    static function model($id)
    {
        $modelname = get_called_class();
        $item = Art::app()->db->query(
            'select * from '.$modelname::$table.' where '.$modelname::$primary.'= ?',
            array($id)
        )->row();
        $model = new $modelname();
        $model->attributes = $item;

        return $model;
    }
}