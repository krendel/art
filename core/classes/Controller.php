<?php

/**
 * Class Controller
 */
class Controller extends Singleton
{
    /**
     * @var string
     */
    public $tplPath = '';
    /**
     * @var string
     */
    public $tplControllerPath = '';
    /**
     * @var array
     */
    private $assets = array();

    /**
     * Controller constructor.
     */
    public function __construct()
    {
        $this->tplPath = APP.'views/';
        $this->tplControllerPath = APP.'views/'.strtolower(str_replace('Controller', '', get_called_class())).'/';
    }

    /**
     * @param $methodName
     * @param array $args
     * @return mixed
     * @throws Except
     */
    public function __call($methodName, $args = array())
    {
        if (is_callable(array($this, $methodName))) {
            return call_user_func_array(array($this, $methodName), $args);
        } else {
            throw new Except('In controller '.get_called_class().' method '.$methodName.' not found!');
        }
    }


    /**
     * renderPartial - метод доступный в контроллере, для вывода файла шаблона.
     * Не запускает больше никаких файлов. Удобен при ajax вызове контроллера
     *
     * @params $filename - название шаблона в папке views/название контроллера/{}.php
     * @params $variables - ключи массива будут доступны в шаблоне как переменные с
     * теми же именами
     * @params $output - если указать false, то данные из шаблона не будут выведены в основной поток а будут возвращены методом
     */
    public function renderPartial($filename, $variables = array(), $output = true)
    {
        $file = $this->tplControllerPath.str_replace('..', '', $filename).'.php';

        return $this->_renderPartial($file, $variables, $output);
    }

    /**
     * render - метод выполняет полный вывод страницы на экран. При этом в нее включается
     * содержимое файла шаблона $filename
     *
     * @params - все параметры идентичны renderPartial
     */
    public function render($filename, $variables = array(), $output = true)
    {
        return $this->renderPartial($filename, $variables, $output);
    }

    /**
     * @param $content
     * @throws Except
     */
    public function renderPage($content)
    {
        $html = $this->_renderPartial($this->tplPath.'main.php', array('content' => $content), false);
        $output = array('head' => '', 'body' => '');
        foreach ($this->assets as $item) {
            if ($item['asset'] == 'script') {
                if ($item['type'] == 'inline') {
                    $output[$item['where']] .= '<script type="text/javascript">'.$item['data'].'</script>'."\n";
                } else {
                    $output[$item['where']] .= '<script type="text/javascript" src="'.$item['data'].'"></script>'."\n";
                }
            } else {
                if ($item['type'] == 'inline') {
                    $output[$item['where']] .= '<style>'.$item['data'].'</style>'."\n";
                } else {
                    $output[$item['where']] .= '<link rel="stylesheet" href="'.$item['data'].'" type="text/css" />'."\n";
                }
            }
        }
        if ($output['head']) {
            $html = preg_replace('#(<\/head>)#iu', $output['head'].'$1', $html);
        }
        if ($output['body']) {
            $html = preg_replace('#(<\/body>)#iu', $output['body'].'$1', $html);
        }
        echo $html;
    }


    /**
     * @param $link
     * @param string $where
     */
    public function addScript($link, $where = 'head')
    {
        $this->addAsset($link, $where);
    }

    /**
     * @param $link
     * @param string $where
     */
    public function addStyleSheet($link, $where = 'head')
    {
        $this->addAsset($link, $where, 'style');
    }

    /**
     * @param $data
     * @param string $where
     */
    public function addScriptDeclaration($data, $where = 'head')
    {
        $this->addAsset($data, $where, 'script', 'inline');
    }

    /**
     * @param $data
     * @param string $where
     */
    public function addStyleSheetDeclaration($data, $where = 'head')
    {
        $this->addAsset($data, $where, 'style', 'inline');
    }

    /**
     * @param $fullpath
     * @param array $variables
     * @param bool $output
     * @return bool|string
     * @throws Except
     */
    private function _renderPartial($fullpath, $variables = array(), $output = true)
    {
        extract($variables);

        if (file_exists($fullpath)) {
            if (!$output) {
                ob_start();
            }
            include $fullpath;

            return !$output ? ob_get_clean() : true;
        } else {
            throw new Except('File '.$fullpath.'.php not found');
        }

    }

    /**
     * @param $link
     * @param string $where
     * @param string $asset
     * @param string $type
     */
    private function addAsset($link, $where = 'head', $asset = 'script', $type = 'url')
    {
        $hash = md5('addScript'.$link.$where.$asset.$type);
        $where = $where == 'head' ? 'head' : 'body';
        $asset = $asset == 'script' ? 'script' : 'style';
        if (!isset($this->assets[$hash])) {
            $this->assets[$hash] = array('where' => $where, 'asset' => $asset, 'type' => $type, 'data' => $link);
        }
    }
}