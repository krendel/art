<?php

return array(
    'sitename' => 'Art',
    'encode' => 'utf-8',
    'cookietime' => 3600,
    'default_module' => 'index',
    'default_controller' => 'index',
    'default_action' => 'index',
    'db' => array(),
    'router' => array(
        '([a-z0-9+_\-]+)/([a-z0-9+_\-]+)/([0-9]+)' => '$controller/$action/$id',
        '([a-z0-9+_\-]+)/([a-z0-9+_\-]+)' => '$controller/$action',
        '([a-z0-9+_\-]+)(/)?' => '$controller',
    ),
    'scripts' => array(),
    'styles' => array(),
);